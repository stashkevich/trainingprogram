'use strict';

/*
Вариант для названия
var trainingProgram ? trainingProgram :{};
trainingProgram.calendarEvents = (function...
*/

var calendarEvents = (function () {

    var subscribers = {
        EVENT_ADDED_TO_QUEUE: [],
        EVENT_UPDATE: [],
        EVENT_IS_SHOWN: [],
        EVENT_REMOVED: [],
    };

    var config = {
        'callFunction': 'privateCallFunction'
    }

    var idGenerator = new privateIDGenerator('id');

    // events list
    var events = [];

    var eventTimer;

    function fireEvent(eventName, event) {
        subscribers[eventName].forEach(function (callback) {
            callback(event);
        });
    }

    var UserEvent = function (title, date, callFunction) {
        this.id = idGenerator.getID();
        this.title = title;
        this.date = date;
        this.isShown = false;
        this.callFunction = callFunction;
    }

    function publicCreateEvent(title, date, callFunction) {

        if (!title) {
            return console.log('Введите название события');
        }

        var currentDate = new Date();

        if (!date) {
            var eventDate = currentDate;
        } else {
            var eventDate = new Date(date);
        }

        if (isNaN(eventDate)) {
            return console.log('Вы ввели некорректную дату для события !');
        }

        if (!callFunction) {
            var eventFunction = config.callFunction;
        } else {
            eventFunction = callFunction;
        }

        return new UserEvent(title, eventDate, eventFunction);
    }

    function privateIDGenerator(prefix) {
        this.prefix = prefix;
        this.num = 1;
        this.getID = function() {
            return this.prefix + (this.num++);
        }
    }

    function privateCompareDate(firstEvent, secondEvent) {
        return firstEvent.date - secondEvent.date;
    }

    function privateRunEventsTimer() {

        if (eventTimer) {
            clearTimeout(eventTimer);
        }

        var currentDate = new Date();

        var recentEvents = privateGetEventsEndOrOlder(currentDate);

        if (recentEvents.length > 0) {
            recentEvents.forEach(function(event, i, recentEvents){
                privateCallFunction(event);
                fireEvent('EVENT_IS_SHOWN', event);
            });
            recentEvents.lenght = 0;
        }

        var nearEvent = privateGetNearEvent(currentDate);

        if (nearEvent.length > 0) {
            var timeToNextEvent = nearEvent[0].date - currentDate;
            eventTimer = setTimeout(function() {
                eval(nearEvent[0].callFunction + '(nearEvent[0])');
                privateMarkShown(nearEvent[0].id);
                fireEvent('EVENT_IS_SHOWN', nearEvent[0]);
                privateRunEventsTimer();
            }, timeToNextEvent);
        }
    }

    function privateGetEventsEndOrOlder(currentDate) {

        var pastEvent = events.filter(function (event) {
            if ((!event.isShown)&&(event.date <= currentDate)) {
                event.isShown = true;
                return true;
            }
            return false;
        });

        return pastEvent;
    }

    function privateGetNearEvent(currentDate) {
        var nearEvent = [];

        for (var i = 0; i < events.length; i++) {
            if ((!events[i].isShown) && (events[i].date >= currentDate)) {
                nearEvent.push(events[i]);
                break;
            }
        }
        return nearEvent;
    }

    function privateMarkShown(evendId) {
        for (var i = 0; i < events.length; i++) {
            if (events[i].id === evendId) {
                events[i].isShown = true;
                break;
            }
        }
    }

    function privateCallFunction(event) {
        alert('Заголовок: ' + event.title + '    Напоминание для события c id: ' + event.id + ' Date: ' + event.date);
    }

    function publicAddEventToQueue(addEvent) {

        var event = {};

        if (arguments.length === 3) {
            event = publicCreateEvent(arguments[0], arguments[1], arguments[2]);
        } else {
            event = addEvent;
        }

        if (events.length > 0) {
            var notCoincidence = true;
            for (var i = 0; i < events.length; i++) {
                if (events[i].date >= event.date) {
                    events.splice(i, 0, event);
                    notCoincidence = false;
                    break;
                }
            }
            if (notCoincidence) {
                events.push(event);
            }
        } else {
            events.push(event);
        }

        fireEvent('EVENT_ADDED_TO_QUEUE', event);

        privateRunEventsTimer();

    }

    function publicGetEvents() {

        var currentDate = new Date();

        function getWeekNumber(date) {
            var target = new Date(date.valueOf()),
                dayNumber = (date.getUTCDay() + 6) % 7,
                firstThursday;

            target.setUTCDate(target.getUTCDate() - dayNumber + 3);
            firstThursday = target.valueOf();
            target.setUTCMonth(0, 1);

            if (target.getUTCDay() !== 4) {
                target.setUTCMonth(0, 1 + ((4 - target.getUTCDay()) + 7) % 7);
            }

            return Math.ceil((firstThursday - target) /  (7 * 24 * 3600 * 1000)) + 1;
        }

        return {
            'all': function () {
                console.log(events);
                return events;
            },
            'day': function () {
                var dayFilterEvent = events.filter(function (event) {
                    return event.date.getDate() === currentDate.getDate();
                });
                return dayFilterEvent;
            },
            'week': function () {
                var weekFilterEvent = events.filter(function (event) {
                    return  getWeekNumber(event.date) === getWeekNumber(currentDate);
                });
                return weekFilterEvent;
            },
            'month': function () {
                var monthFilterEvent = events.filter(function (event) {
                    return event.date.getMonth() === currentDate.getMonth();
                });
                return monthFilterEvent;
            },
            'period': function (startDate, endDate) {
                var startPeriodDate = Date.parse(startDate);
                var endPeriodDate = Date.parse(endDate);
                if (isNaN(startPeriodDate) || isNaN(endDate)) {
                    return console.log('Вы ввели некорректную дату для события !');
                }
                var periodFilterEvent = events.filter(function (event) {
                    if ((Date.parse(event.date) >= startPeriodDate) && (Date.parse(event.date) <= endPeriodDate)) {
                        return event;
                    }
                });
                return periodFilterEvent;
            },
            'getWeekNumber': getWeekNumber,
            'byId': function (id) {
                if (!id) {
                    console.log('Вы не указали id события !');
                    return false;
                }
                for (var i = 0; i < events.length; i++) {
                    if (events[i].id === id){
                        return {
                            'index': i,
                            'event': events[i]
                        };
                    }
                }
                console.log('Нет события с указанным id !');
                return false;
            }
        }
    }

    function publicUpdateEvent(id, title, date) {
        if (!id) {
            return console.log('Вы не ввели идентификатор обновляемого события');
        }

        if (date){
            var setDate = new Date(date);
            if (isNaN(setDate)) {
                return console.log('Вы ввели некорректную дату для события !');
            }
        }

        for (var i = 0; i < events.length; i++) {
            if (events[i].id === id) {
                if (title) {
                    events[i].title = title;
                }

                if (setDate) {
                    events[i].date = setDate;
                    events[i].isShown = false;
                }
                events.sort(privateCompareDate);
                privateRunEventsTimer();
                fireEvent('EVENT_UPDATE', events[i]);
                return console.log('Все изменения сохранены !');
            }
        }
        return console.log('Нет события с id равным ' + id + ' !');
    }

    function  publicDeleteEvent(id) {

        if (!id) {
            return console.log('Вы не ввели идентификатор события, которое нужно удалить');
        }

        var deleted = false;

        for (var i = 0; i < events.length; i++){
            if (events[i].id === id) {
                fireEvent('EVENT_REMOVED', events[i]);
                events.splice(i, 1);
                deleted = true;
                break;
            }
        }

        if (deleted) {
            console.log('Элемент с id: ' + id + ' удален !');
            privateRunEventsTimer();
        } else {
            console.log('Нет события с id равным ' + id + ' !');
        }

    }

    function publicSetEventProperties(id, properties) {
        if (!id) {
            return console.log('Вы не ввели идентификатор обновляемого события');
        }
        var event = publicGetEvents().byId(id);
        if (event) {
            Object.defineProperties(events[event.index], properties);
        }
    }

    function publicAddSubscriber (eventName, callback) {
        subscribers[eventName].push(callback);
    }

    function publicSetConfigProperties(properties) {
        Object.defineProperties(config, properties);
    }

    function publicGetModuleConfig() {
        return config;
    }

    return {
        'createEvent': publicCreateEvent,
        'addEventToQueue' : publicAddEventToQueue,
        'updateEvent' : publicUpdateEvent,
        'getEvents': publicGetEvents,
        'deleteEvent': publicDeleteEvent,
        'addSubscriber': publicAddSubscriber,
        'setEventsProperty': publicSetEventProperties,
        'setConfigProperties': publicSetConfigProperties,
        'getModuleConfig': publicGetModuleConfig
    }

})();