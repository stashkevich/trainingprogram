'use strict';
/*
 * Расширения для модуля "Календарь событий"
 * Позваляет задавать повторяющиеся с определенной периодичностью события.
 * */
var calendarEvents = (function(calendarEvents){

    function privateCompareNumeric(a, b) {
        if (a > b) return 1;
        if (a < b) return -1;
    }

    function getDayInWeek (week, day, year) {
        var dayInWeek = new Date(year, 0, 7 * week);
        dayInWeek.setDate(dayInWeek.getDate()-(dayInWeek.getDay() || 7) + day);
        return dayInWeek;
    }

    calendarEvents.addRecurringEvents = function (title, date, callFunction, recurringDay) {
        var recurringEvent = calendarEvents.createEvent(title, date, callFunction);
        recurringEvent.isRecurring = true;

        if (recurringDay instanceof Array) {
            if(recurringDay.length > 0) {
                recurringEvent.recurringDays = recurringDay.sort(privateCompareNumeric);
            } else {
                recurringEvent.recurringDays = 'all';
            }
        } else {
            recurringEvent.recurringDays = 'all';
        }

        console.log('addReq');
        calendarEvents.addEventToQueue(recurringEvent);
    };

    calendarEvents.addSubscriber('EVENT_IS_SHOWN', function() {

        alert('Recurring EVENT_IS_SHOWN');

        var events = calendarEvents.getEvents().all().slice();

        if (events.length > 0) {
            var pastEvents = events.filter(function(event){
                return (event.isRecurring)&&(event.isShown);
            });

            pastEvents.forEach(function(event){

                var dateNextNotification;

                if (event.recurringDays === 'all') {
                    dateNextNotification = new Date(Date.parse(event.date) + 86400000);
                } else {
                    var currentDate = new Date();
                    var currentWeek = calendarEvents.getEvents().getWeekNumber(currentDate);
                    var currentDay = currentDate.getDay();
                    var currentYear = currentDate.getFullYear();

                    var eventHours = event.date.getHours();
                    var eventMinutes = event.date.getMinutes();
                    var eventSeconds = event.date.getSeconds();

                    var possibleRecurringDays = event.recurringDays.filter(function(number) {
                        return number > currentDay;
                    });

                    if (possibleRecurringDays.length > 0) {
                        dateNextNotification = getDayInWeek(currentWeek, possibleRecurringDays[0], currentYear);
                    } else {
                        var nextWeek = new Date(Date.parse(currentDate) + 604800000);
                        var nextWeekNumber = calendarEvents.getEvents().getWeekNumber(nextWeek);
                        dateNextNotification = getDayInWeek(nextWeekNumber, event.recurringDays[0], nextWeek.getFullYear());
                    }

                    dateNextNotification.setHours(eventHours);
                    dateNextNotification.setMinutes(eventMinutes);
                    dateNextNotification.setSeconds(eventSeconds);
                }
                calendarEvents.updateEvent(event.id, '', dateNextNotification);
            });
            pastEvents.length = 0;
        }
    });

    return calendarEvents;

})(calendarEvents || {});
